import React from 'react';
//import logo from './logo.svg';
import './App.css';
//import Header from './components/Header'

function App() {
  return (

    <div>
      <header className="title">
        <h1>css grid</h1>
      </header>

      <div className="container">
      
        <header className="header">
          <h1>LOGO</h1>
        </header>
        
        <main className="content">
          <article className="article">
            <h1>Title</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, mollitia?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident mollitia autem consequuntur! Rem sapiente magni unde numquam, soluta quibusdam impedit!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, dolorem.</p>
          </article>
          <article className="article">
            <h1>Title</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, mollitia?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident mollitia autem consequuntur! Rem sapiente magni unde numquam, soluta quibusdam impedit!</p>
          </article>
          <article className="article">
            <h1>Title</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, mollitia?</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident mollitia autem consequuntur! Rem sapiente magni unde numquam, soluta quibusdam impedit!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, dolorem.</p>
          </article>
        </main>
        
        <aside className="sidebar">
          <div>
            <img src="http://lorempixel.com/300/200/city" alt="1"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, quasi.</p>
          </div>
          <div>
            <img src="http://lorempixel.com/300/200/food" alt="2"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, quasi.</p>
          </div>
          <div>
            <img src="http://lorempixel.com/300/200/people" alt="3"/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, quasi.</p>
          </div>
        </aside>
        
        <div className = "related-post">
          Another Post
        </div>
        <div className = "related-post">
          Another Post
        </div>
        <div className = "related-post">
          Another Post
        </div>
        <div className = "related-post">
          Another Post
        </div>
        <div className = "related-post">
          Another Post
        </div>

        <footer className="footer">
          <h1>Footer</h1>
          <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Temporibus, dolores.</p>
        </footer>


    </div>
    </div>
  );
}

export default App;
